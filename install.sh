#!/bin/bash

# config file


#!/bin/bash

# ask for password up-front.
sudo -v

# add https 
sudo apt-get install apt-transport-https

# atom
read -p "Install Atom (Y/y)" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]; then
    wget https://atom.io/download/deb -O atom-amd64.deb
    sudo dpkg -i atom-amd64.deb

    # atom package sync
    # apm install package-sync
fi

# add repos
echo "add sublime repo"

# add sublime text repo
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list

# basic update 
sudo apt-get update -y
sudo apt-get dist-upgrade 

# install apps

echo "===================="
echo "    INSTALL APPS    "
echo "===================="

sudo apt-get -y install \
    sublime-text git terminator fish \
    curl filezilla vim-gtk \
    gimp p7zip p7zip-full p7zip-rar tree \

# set fish as default shell
chsh -s /usr/bin/fish
  
# Install my dotfiles
echo ""
echo "========================="
echo "   Symlinking dotfiles   "
echo ""

link() {
  from="$1"
  to="$2"
  echo "Linking '$from' to '$to'"
  rm -f "$to"
  ln -s "$from" "$to"
}

# link ~/dotfiles/bash_profile ~/.bash_profile
# link ~/dotfiles/bashrc ~/.bashrc
# link ~/dotfiles/railsrc ~/.railsrc
# link ~/dotfiles/zprofile ~/.zprofile
# link ~/dotfiles/zshrc ~/.zshrc
# link ~/dotfiles/xsession.rc ~/.xsession.rc
# link ~/dotfiles/tmux.conf ~/.tmux.conf
# link ~/dotfiles/gitconfig ~/.gitconfig
# link ~/dotfiles/gitignore ~/.gitignore
# link ~/dotfiles/pryrc ~/.pryrc
# link ~/dotfiles/vim ~/.vim
# link ~/dotfiles/vimrc ~/.vimrc
# link ~/dotfiles/atom ~/.atom



# Sublime Text 3
mkdir ~/.config/sublime-text-3/
unzip -d ~/.config/sublime-text-3/ ./data/sublime-text-3.zip
cp -ar ./data/sublime-text-3/* ~/.config/sublime-text-3/


# fonts
# mkdir ~/.fonts
# cp -ar ./data/fonts/* ~/.fonts/

# create folders
mkdir ~/Projects


# Set initial Git config options

# git config --global user.name "MapMonkey"
# git config --global user.email johndoe@gmail.com
# git config --global core.excludesfile ~/.gitignore



# update system settings

# requires clicks
sudo apt-get install -y ubuntu-restricted-extras

# prompt for a reboot
clear
echo ""
echo "===================="
echo " TIME FOR A REBOOT! "
echo "===================="
echo ""